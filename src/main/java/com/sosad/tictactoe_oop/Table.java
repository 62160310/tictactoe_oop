/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sosad.tictactoe_oop;

/**
 *
 * @author Bunny0_
 */
public class Table {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    Player playerX;
    Player playerO;
    Player currentPlayer;
    Player winner;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        currentPlayer = X;
    }

    void resetTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                table[i][j] = '-';
            }
        }
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println(" ");
        }

    }

    public boolean setRowCol(int row, int col) {
        // put_table
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getPlayer();
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {

        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    public boolean checkWin() {
        if (checkHorizontal()) {
            return true;
        }
        if (checkVertical()) {
            return true;
        }
        if (checkTopleft()) {
            return true;
        }
        if (checkTopright()) {
            return true;
        }
        return false;
    }

    boolean checkHorizontal() {
        for (int i = 0; i < table.length; i++) {
            //checkhorizontal
            if (table[i][0] == table[i][1] && table[i][0] == table[i][2]
                    && table[i][0] != '-') {
                return true;
            }
        }
        return false;
    }

    static boolean checkVertical() {
        for (int i = 0; i < table.length; i++) {
            //checkvertical
            if (table[0][i] == table[1][i] && table[0][i] == table[2][i]
                    && table[0][i] != '-') {
                return true;
            }
        }
        return false;
    }

    static boolean checkTopleft() {
        for (int i = 0; i < table.length; i++) {
            //check_top left>bottom right
            if (table[0][0] == table[1][1] && table[0][0] == table[2][2]
                    && table[0][1] != '-') {
                return true;

            }
        }
        return false;
    }

    static boolean checkTopright() {
        for (int i = 0; i < table.length; i++) {
            //check_top right>bottom left
            if (table[2][0] == table[1][1] && table[2][0] == table[0][2]
                    && table[2][0] != '-') {
                return true;
            }
        }
        return false;
    }

    public Player getWinner() {
        return winner;
    }
}
