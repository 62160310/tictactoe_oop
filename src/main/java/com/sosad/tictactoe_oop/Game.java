/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sosad.tictactoe_oop;

import java.util.Scanner;

/**
 *
 * @author Bunny0_
 */
public class Game {

    Scanner kb = new Scanner(System.in);
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    public int draw = 0;
    int row, col;
    int checkdraw;
    boolean play = true;
    static boolean isFinish = false;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);

    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    void runGame() {
        this.showWelcome();
        table.resetTable();
        do {
            this.showTable();
            this.showTurn();
            this.Input();
            draw += 1;
            if (checkFinishGame()) {
                break;
            }
            table.switchPlayer();
        } while (!isFinish);
        this.showTable();
        System.out.println("");
        System.out.println("play the game again?");
        System.out.println("play=>1 or exit=>2");
        int again = kb.nextInt();
        playAgain(again);
    }

    private boolean playAgain(int again) {
        if(again == 1){
            return play = true;
        }
        return play = false;
    }

    private boolean checkFinishGame() {
        if (draw == 9) {
            this.showTable();
            System.out.println("Draw!");
            return true;
        }
        if (table.checkWin() != false) {
            this.showTable();
            showResult();
            return true;
        }
        return false;

    }

    void Input() {
        while (true) {
            System.out.println("Please input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error This position cannot be entered ");
        }
    }

    void showTurn() {
        System.out.println(table.getCurrentPlayer().getPlayer() + " turn");
    }

    void showResult() {
        System.out.println("Player " + table.currentPlayer.player + " Win...");
    }

    void showBye() {
        System.out.println("ByeBye ....");
    }
}
