/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sosad.tictactoe_oop;

/**
 *
 * @author Bunny0_
 */
public class Player {

    char player;
    int win;
    int lose;
    int draw;

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getLose() {
        return lose;
    }

    public void setLose(int lose) {
        this.lose = lose;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }
    //get set player
    public char getPlayer() {
        return this.player;
    }

    public Player(char player) {
        this.player = player;
    }

    

}
