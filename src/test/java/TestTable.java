/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.sosad.tictactoe_oop.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Bunny0_
 */
public class TestTable {
    
    public TestTable() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
     public void testHorizontal1_ByX(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 0);
         table.setRowCol(0, 1);
         table.setRowCol(0, 2);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(x, table.getCurrentPlayer());
     }
     @Test
     public void testHorizontal2_ByX(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(x,o);
         table.setRowCol(1, 0);
         table.setRowCol(1, 1);
         table.setRowCol(1, 2);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(x, table.getCurrentPlayer());
     }
     @Test
     public void testHorizontal3_ByX(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(x,o);
         table.setRowCol(2, 0);
         table.setRowCol(2, 1);
         table.setRowCol(2, 2);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(x, table.getCurrentPlayer());
     }
     @Test
     public void testVertical1_ByO(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(o,x);
         table.setRowCol(0, 0);
         table.setRowCol(1, 0);
         table.setRowCol(2, 0);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(o, table.getCurrentPlayer());
     }
     @Test
     public void testVertical2_ByO(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(o,x);
         table.setRowCol(0, 1);
         table.setRowCol(1, 1);
         table.setRowCol(2, 1);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(o, table.getCurrentPlayer());
     }
     @Test
     public void testVertical3_ByO(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(o,x);
         table.setRowCol(0, 2);
         table.setRowCol(1, 2);
         table.setRowCol(2, 2);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(o, table.getCurrentPlayer());
     }
     @Test
     public void testdiagonalleft_ByX(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 0);
         table.setRowCol(1, 1);
         table.setRowCol(2, 2);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(x, table.getCurrentPlayer());
     }
     @Test
     public void testdiagonalright_ByX(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 2);
         table.setRowCol(1, 1);
         table.setRowCol(2, 0);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(x, table.getCurrentPlayer());
     }
     @Test
     public void testdiagonalleftt_ByO(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(o,x);
         table.setRowCol(0, 0);
         table.setRowCol(1, 1);
         table.setRowCol(2, 2);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(o, table.getCurrentPlayer());
     }
     @Test
     public void testdiagonalright_ByO(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(o,x);
         table.setRowCol(0, 2);
         table.setRowCol(1, 1);
         table.setRowCol(2, 0);
         table.checkWin();
         assertEquals(true,table.checkWin());
         assertEquals(o, table.getCurrentPlayer());
     }
     @Test
     public void testRowColumnIsEmpty(){
        Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(x,o);
         table.setRowCol(0, 0);
         assertEquals(false,table.setRowCol(0, 0));
     }
     @Test
     public void testDraw(){
         Game game = new Game();
         Player x = new Player('x');
         Player o = new Player('o');
         Table table = new Table(x,o);         
         table.setRowCol(0, 0);
         game.draw++;
         table.setRowCol(0, 1);
         game.draw++;
         table.setRowCol(1, 2);
         game.draw++;
         table.setRowCol(2, 0);
         game.draw++;
         table.setRowCol(2, 1);
         game.draw++;
         table.checkWin();
         table.switchPlayer();
         table.setRowCol(0, 2);
         game.draw++;
         table.setRowCol(1,0);
         game.draw++;
         table.setRowCol(1,1);
         game.draw++;
         table.setRowCol(2,2);
         game.draw++;
         assertEquals(true,table.checkWin());
         assertEquals(9, game.draw);
         
     }
     
     
}
